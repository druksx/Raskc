
## About me

## Stats
<p align = "center">
  <img src = "https://github-readme-stats.vercel.app/api?username=Raskc&show_icons=true&theme=radical&line_height=27">
  <img src = "https://github-readme-stats.vercel.app/api/top-langs/?username=Raskc&theme=radical&line_height=27">
</p>
<p align = "center">
  <img src="https://github-readme-streak-stats.herokuapp.com/?user=Raskc&show_icons=true&locale=en&layout=compact&theme=radical&line_height=0" />
</p>
## Language
<p align = "left">
  <img src="https://img.shields.io/badge/lua-%232C2D72.svg?style=for-the-badge&logo=lua&logoColor=white" />
  <img src="https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54" />
  <img src="https://img.shields.io/badge/c-%2300599C.svg?style=for-the-badge&logo=c&logoColor=white" />
</p>
<h2 align="left">Contacts <img src="assets/contacts.gif" width="30"></h2>
<p align="left">
  <a href="https://twitter.com/Rask_Dev"><img alt="Twitter" height="50" width="50" src="assets/twitter.png"></a>
</p>
